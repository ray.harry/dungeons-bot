from abc import ABC


class Item(ABC):
    """An item carried by some creature. Could be gold, magic items, equipment,
    etc.
    """

    def __init__(self, index: str, name: str, emoji = "", cost = None) -> None:
        """Create a new Item.

        `index`: lower-case index of the item.
        `name`: title-case name of the item.
        `emoji`: emoji of the Item, if any.
        `cost`: cost of the Item, if any.
        """
        super().__init__()

        self.index = index
        self.name =  name
        self.cost = cost
        self.emoji = emoji


    def nice_name(self) -> str:
        """The name of the `Item` in a nice, human-readable format.
        """
        return self.name
