from enum import Enum

class EffectMod(Enum):
    """How the effect interacts with the base stat of the creature.
    """
    CONSTANT = "constant"
    ADD = "add"
    SUB = "sub"
    MULT = "mult"
    DIV = "div"
    MAX = "max"


class EffectType(Enum):
    """The nature of the effect, whether positive, neutral, or negative.
    """
    POSITIVE = 1
    NEUTRAL = 0
    NEGATIVE = -1


class EffectStat(Enum):
    """The stat the effect changes.
    """
    STRENGTH = "str"
    HEALING = "healing"
    TEMP_HP = "temp_hp"
    ATTACK_ROLL = "attack_roll"
    DAMAGE_ROLL = "damage_roll"
    SAVING_THROW = "saving_throw"
    SAVING_THROW_ADVANTAGE = "saving_throw_advantage"
    WALK_SPEED = "walk_speed"
    FLY_SPEED = "fly_speed"
    SWIM_SPEED = "swim_speed"
    BURROW_SPEED = "burrow_speed"
    CLIMB_SPEED = "climb_speed"
    VISIBILITY = "visibility"
    RESISTANCE = "resistance"
    IMMUNITY = "immunity"
    VULNERABILITY = "vulnerability"
    ARMOR_CLASS = "armor_class"
    ACTION_COUNT = "action_count"


class EffectEnd(Enum):
    """Ways in which the effect is stopped.
    """
    DURATION = "duration"
    ATTACK = "attack"
    SPELL = "spell"
    NEVER = "never"


class Effect:
    """A status effect that changes a creature's stats in some way.
    """
    def __init__(
            self,
            effect_type: EffectType,
            effect_mod: EffectMod,
            effect_stat: EffectStat,
            value,
            duration: int | None = 3600,
            effect_ends: set[EffectEnd] | None = None,
            effect_source = None
        ):
        """Create a new Effect.

        `effect_type`: the positive/negative nature of the effect.
        `effect_mod`: how the effect modifies the base stat.
        `effect_stat`: the stat the effect modifies.
        `value`: the effect modifier.
        `duration`: the duration of the effect in seconds. Defaults to 1 hour. Use None for indefinite (must be removed manually).
        `effect_ends`: ways in which the effect will end. If `duration` is specified, `EffectEnd.DURATION` is automatically added.
        """
        self.effect_type = effect_type
        self.effect_mod = effect_mod
        self.effect_stat = effect_stat
        self.value = value
        self.duration = duration
        self.effect_ends = effect_ends or set()
        if effect_ends is not None:
            self.effect_ends = set(effect_ends)
        if duration is not None:
            self.effect_ends.add(EffectEnd.DURATION)
        self.effect_source = effect_source


    def mod_str(self) -> str:

        match self.effect_mod:
            case EffectMod.CONSTANT:
                return f"{self.value}"

            case EffectMod.ADD:
                return f"+{self.value}"

            case EffectMod.SUB:
                return f"-{self.value}"

            case EffectMod.MULT:
                return f"*{self.value}"

            case EffectMod.DIV:
                return f"/{self.value}"

            case EffectMod.MAX:
                return f"^{self.value}"


    def has_effect_end(self, end: EffectEnd) -> bool:

        return end in self.effect_ends


    @staticmethod
    def reduce(base_stat: int, effects: list["Effect"]) -> "Effect":
        """Reduce the given effects into a single effect, which is always `EffectMod.Constant`.
        All effects should modify the same `EffectStat`.
        Duration will be the minimum duration of the effects.
        """
        stat = effects[0].effect_stat
        duration = effects[0].duration
        for effect in effects:
            base_stat = Effect.apply_effect(base_stat, effect)
            assert effect.duration
            assert duration
            duration = min(duration, effect.duration)
            if stat != effect.effect_stat:
                raise ValueError("Effects are not all of the same EffectStat.")

        effect_type = EffectType.NEUTRAL
        if all(e.effect_type == EffectType.POSITIVE for e in effects):
            effect_type = EffectType.POSITIVE
        elif all(e.effect_type == EffectType.NEGATIVE for e in effects):
            effect_type = EffectType.NEGATIVE

        return Effect(effect_type, EffectMod.CONSTANT, effects[0].effect_stat, base_stat)


    @staticmethod
    def apply_effect(stat: int, effect: "Effect") -> int:
        """Calculate the stat value from a single effect.
        """
        mult_div = 1
        add_sub = 0
        max_effect = stat
        match effect.effect_mod:
            case EffectMod.CONSTANT:
                return effect.value

            case EffectMod.ADD:
                add_sub += effect.value

            case EffectMod.SUB:
                add_sub -= effect.value

            case EffectMod.MULT:
                mult_div *= effect.value

            case EffectMod.DIV:
                mult_div /= effect.value

            case EffectMod.MAX:
                max_effect = max(max_effect, effect.value)

        return (max_effect * mult_div) + add_sub

    @staticmethod
    def apply_effects(base_stat: int, effects: list["Effect"]) -> int:
        """Calculate the stat value from a given list of effect modifiers.

        `base_stat`: the base stat.
        `effects`: list of Effects that modify the given base stat.

        Returns the modified stat.
        """
        mult_div = 1
        add_sub = 0
        max_effect = base_stat
        for effect in effects:
            match effect.effect_mod:
                case EffectMod.CONSTANT:
                    return effect.value

                case EffectMod.ADD:
                    add_sub += effect.value

                case EffectMod.SUB:
                    add_sub -= effect.value

                case EffectMod.MULT:
                    mult_div *= effect.value

                case EffectMod.DIV:
                    mult_div /= effect.value

                case EffectMod.MAX:
                    max_effect = max(max_effect, effect.value)

        return (max_effect * mult_div) + add_sub
