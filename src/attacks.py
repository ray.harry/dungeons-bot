from enum import Enum
import random

from dice import Dice
from dnd.dndclient import ActionOutcome
from dnd.equipment import Equipment
from dnd.spell import Spell
import util


class AttackRange(Enum):
    """Range of an attack, either Melee, Ranged, or Mixed.
    """
    MELEE = 0
    RANGED = 1
    MIXED = 2
    SPELL = 3

    @staticmethod
    def combine(range_1: "AttackRange", range_2: "AttackRange") -> "AttackRange":
        """Find the combination of two ranges.

        `range_1`: first `AttackRange`.
        `range_2`: second `AttackRange`.

        melee + ranged = mixed,
        melee + melee = melee,
        ranged + ranged = ranged
        """
        if range_1 == range_2:
            return range_1

        return AttackRange.MIXED


    def __str__(self) -> str:

        return str(self.__dict__)


class Attack:

    def __init__(self, name: str, damage_dice: list[Dice], damage_types: list[str], range: AttackRange | None, attack_modifier: int, damage_modifier: int, proficient: bool) -> None:
        """Create an arbitrary Attack.

        `damage_dice`: damage Dice to roll for the attack.
        `damage_type`: damage type of the attack.
        `range`: the range of the attack.
        `attack_modifier`: modifier to use for the attack roll.
        `damage_modifier`: modifier to use for the damage roll.
        """
        self.name = name
        self.damage_dice = damage_dice
        self.damage_types = damage_types
        self.range = range
        self.proficient = proficient
        self.attack_modifier = attack_modifier
        self.damage_modifier = damage_modifier
        self.hands = 0


    def max_damage(self) -> int:

        return sum(d.max() for d in self.damage_dice)


    def min_damage(self) -> int:

        return sum(d.min() for d in self.damage_dice)


    def mean_damage(self) -> float:

        return sum(d.mean() for d in self.damage_dice)


    def __str__(self) -> str:

        return str(self.__dict__)


class AttackMonster(Attack):

    def __init__(self, action: dict) -> None:
        """Create an Attack from a Monster action.

        `action`: the Monster's action dict
        """
        self._dnd_dict = action
        self.dc_type = ""
        self.dc_value = 0.0
        self.dc_modifier = 1.0

        damage_dice = []
        damage_types = []
        attack_bonus = 0
        if "damage" in action:
            # damage
            for damage in action["damage"]:
                # choose a random damage
                if "choose" in damage:
                    for _ in range(damage["choose"]):
                        damage_choice = random.choice(damage["from"]["options"])
                        damage_dice.append(Dice(damage_choice["damage_dice"]))
                        damage_types.append(damage_choice["damage_type"]["index"])
                # static damage
                else:
                    damage_dice.append(Dice(damage["damage_dice"]))
                    damage_types.append(damage["damage_type"]["index"])

            # attack dc
            if "dc" in action:
                self.dc_type = action["dc"]["dc_type"]["index"]
                self.dc_value = action["dc"]["dc_value"]
                self.dc_modifier = 0.5 if action["dc"]["success_type"] == "half" else 0.0

            # attack bonus
            if "attack_bonus" in action:
                attack_bonus = int(action["attack_bonus"])
        else:
            damage_dice.append(Dice("1"))
            damage_types.append("bludgeoning")

        super().__init__(action["name"], damage_dice, damage_types, None, attack_bonus, 0, True)

    def __str__(self) -> str:

        return str(self.__dict__)


class AttackSpell(Attack):

    def __init__(
            self,
            spell: Spell,
            attack_modifier: int,
            damage_modifier: int,
            slot_level: int,
            caster_level: int,
            dc_value: int
        ) -> None:
        """Create an attack with a Spell.

        `spell`: raw spell dict.
        `slot_level`: slot level used for the spell.
        `caster_level`: level of the Creature that cast the spell.
        """
        damage_dice = Dice("1")
        if "damage_at_character_level" in spell.damage:
            level = util.key_or_less(str(caster_level), spell.damage["damage_at_character_level"])
            damage_dice = Dice(spell.damage["damage_at_character_level"][level])
        if "damage_at_slot_level" in spell.damage:
            level = util.key_or_less(str(slot_level), spell.damage["damage_at_slot_level"])
            damage_dice = Dice(spell.damage["damage_at_slot_level"][level])

        self.is_attack = False
        spell_range = None

        if spell.attack_type:
            self.is_attack = True
            spell_range = AttackRange.RANGED
            if spell.attack_type == "melee":
                spell_range = AttackRange.MELEE
        elif spell.range == "touch":
            spell_range = AttackRange.MELEE
        elif spell.range == "self":
            spell_range = AttackRange.MELEE
            if spell.area_of_effect and spell.area_of_effect["size"] > 15:
                spell_range = AttackRange.RANGED
        elif spell.range == "special":
            spell_range = AttackRange.MIXED
        elif spell.range == "sight":
            spell_range = AttackRange.RANGED
        else:
            feet = int(spell.range.split(" ")[0])
            spell_range = AttackRange.MELEE if feet <= 15 else AttackRange.RANGED

        damage_types = []
        if spell.damage and "damage_type" in spell.damage:
            damage_types = [spell.damage["damage_type"]["index"]]

        super().__init__(spell.name, [damage_dice], damage_types, spell_range, attack_modifier, damage_modifier, True)

        self.spell = spell
        self.caster_level = caster_level
        self.slot_level = slot_level

        self.dc_type = ""
        self.dc_modifier = 1.0
        self.dc_value = dc_value
        if spell.dc:
            self.dc_type = spell.dc_type
            self.dc_modifier = 0.5 if spell.dc_success == "half" else 0.0


    def __str__(self) -> str:
        """String representation of the `AttackSpell`
        """
        return str(self.__dict__)


class AttackWeapon(Attack):

    def __init__(
            self,
            weapon: Equipment,
            damage_dice: Dice,
            damage_type: str,
            attack_modifier: int,
            damage_modifier: int,
            proficient: bool,
            range: AttackRange,
            hands: int
        ) -> None:
        """Create an attack with the described weapon.

        `weapon`: raw weapon dict
        `damage_dice`: damage dice to roll for this attack
        `damage_type`: damage type for this attack
        `modifier`: ability modifier for this account (attack and damage roll bonus)
        `proficient`: whether this attack should get proficiency bonus (attack roll bonus)
        `range`: range of this attack
        """
        super().__init__(weapon.name, [damage_dice], [damage_type], range, attack_modifier, damage_modifier, proficient)
        self._dnd_dict = weapon
        self.hands = hands


    def __str__(self) -> str:
        """String representation of the `AttackWeapon`.
        """
        assert self.range
        return str(self.__dict__)


class AttackSummary:
    """
    Represents an attack and data to perform it.
    """

    def __init__(self, attacks: list[Attack] | None = None, prompt = None) -> None:
        """Create a new attack.

        `attacks`: list of `AttackWeapons` in this attack.
        `prompt`: prompt to display on polls for this attack.
        """
        self.attacks: list[Attack] = []
        if attacks is not None:
            for attack in attacks:
                self.add_attack(attack)

        self.prompt = prompt
        self.emoji = ""


    def get_emoji(self) -> str:

        if self.emoji:
            return self.emoji

        if all(isinstance(a, AttackSpell) for a in self.attacks):
            max_slot = max(a.slot_level for a in self.attacks) # type: ignore[attr-defined]
            return util.emojis["spell_slots"][str(max_slot)]

        if all(isinstance(a, AttackWeapon) for a in self.attacks):

            if self.get_range() == AttackRange.MELEE:
                if all(a.hands == 0 for a in self.attacks):
                    return util.emojis["attacks"]["unarmed"]
                elif len(self.attacks) == 2 or any(a.hands > 1 for a in self.attacks):
                    return util.emojis["attacks"]["melee_2h"]
                else:
                    return util.emojis["attacks"]["melee_1h"]

            if self.get_range() == AttackRange.RANGED:
                return util.emojis["attacks"]["ranged"]

            if self.get_range() == AttackRange.MIXED:
                return util.emojis["attacks"]["mixed"]

        return ""


    def add_attack(self, attack: Attack) -> None:
        """Add an attack.

        `attack_weapon`: `AttackWeapon` to add to this attack.
        """
        self.attacks.append(attack)


    def get_attacks(self) -> list[Attack]:
        """Get attacks.
        """
        return self.attacks


    def set_prompt(self, prompt) -> None:
        """Set the prompt for this attack.

        `prompt`: prompt string to set.
        """
        self.prompt = prompt


    def get_prompt(self) -> str:
        """Get the prompt for this attack.
        """
        return self.prompt or ""


    def get_range(self) -> AttackRange:
        """Get the combined range of all attacks.
        """
        attack_range = self.attacks[0].range
        for attack in self.attacks:
            assert attack_range
            assert attack.range
            attack_range = AttackRange.combine(attack_range, attack.range)
        assert attack_range
        return attack_range


    def __str__(self) -> str:
        """Get string representation of the AttackSummary.
        """
        return self.get_prompt()


    @staticmethod
    def combine(attack_1: "AttackSummary", attack_2: "AttackSummary") -> "AttackSummary":
        """Combine this attack with another, returning a new `AttackSummary` object.

        `attack_1`: the first `AttackSummary`.
        `attack_2`: the second `AttackSummary`.
        """
        attack_summary = AttackSummary()

        for attack in attack_1.get_attacks():
            attack_summary.add_attack(attack)
        for attack in attack_2.get_attacks():
            attack_summary.add_attack(attack)

        return attack_summary


class AttackResult:
    """Represents an attack result, with a damage amount, crit, and name.
    """

    def __init__(self, attacks: list[Attack], damage: int, attack_hit: ActionOutcome, name: str) -> None:
        """Create a new `AttackResult`.

        `attacks`: attacks used, only including attacks that hit.
        `damage`: damage of the attack.
        `attack_hit`: `ActionOutcome` of the attack.
        `name`: name of the attack.
        """
        self.attacks = attacks
        self.damage = damage
        self.attack_hit = attack_hit
        self.name = name


    def __str__(self) -> str:

        return str(self.__dict__)
