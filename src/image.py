# mypy: disable-error-code="no-redef"

import math
import os
import random
from time import sleep
from PIL import Image
from constants import Dimensions, RACE_ADJECTIVES
import util
from loguru import logger
from horde_sdk.ai_horde_api.ai_horde_clients import AIHordeAPISimpleClient
from horde_sdk.ai_horde_api.apimodels import ImageGenerateAsyncRequest, ImageGenerationInputPayload, LorasPayloadEntry

from world import Location, Weather, World


logger.remove()
API_URL = util.config["stablehorde_url"]
# Amount of steps that can be performed without any kudos
MIN_STEPS = 5

DEFAULT_AVATAR_PATH = os.path.join(util.root, "data/avatar.png")
OVERLAY_AVATAR_PATH = os.path.join(util.root, "data/avatar-overlay.png")
AI_AVATAR_PATH = os.path.join(util.root, "data/avatar-overlay-ai.webp")
AI_AVATAR_ORIGINAL_PATH = os.path.join(util.root, "data/avatar-ai.webp")

DEFAULT_HEADER_PATH = os.path.join(util.root, "data/header.png")
OVERLAY_HEADER_PATH = os.path.join(util.root, "data/header-overlay.png")
AI_HEADER_PATH = os.path.join(util.root, "data/header-overlay-ai.webp")
AI_HEADER_ORIGINAL_PATH = os.path.join(util.root, "data/header-ai.webp")

AI_STORY_PATH = os.path.join(util.root, "data/story-ai.webp")

ABILITY_TO_ADJ = {
    "str": [
        "strong",
        "powerful",
        "sturdy",
        "herculean"
    ],
    "dex": [
        "dexterous",
        "agile",
        "nimble",
        "graceful",
        "quick"
    ],
    "con": [
        "hardy",
        "resilient",
        "tough",
        "stalwart"
    ],
    "int": [
        "intelligent",
        "brilliant",
        "knowledgeable",
        "ingenious",
        "intellectual"
    ],
    "wis": [
        "wise",
        "insightful",
        "perceptive",
        "prudent",
        "discerning"
    ],
    "cha": [
        "charismatic",
        "persuasive",
        "captivating",
        "charming",
        "eloquent"
    ]
}


class Description():
    """Describes a story scene for use in AI prompts.
    """
    def __init__(
        self,
        world: World,
        text = "",
        dimensions: tuple[int, int] | None = None,
        style = "",
        weapon = True,
        armor = True,
        # If true, the prompt is supposed to inject the hero description into the prompt
        # If False it should only use the event description
        hero = True,
        location: Location | None = None,
        weather: Weather | None = None
    ) -> None:

        self.text = text
        self.world = world

        self.style = style
        self.weapon = weapon
        self.armor = armor
        self.hero = hero

        self.location: Location
        if location is None:
            self.location = world.location
        else:
            self.location = location

        self.weather: Weather
        if weather is None:
            self.weather = world.weather
        else:
            self.weather = weather

        self.dimensions: tuple[int, int]
        if dimensions is None:
            self.dimensions = Dimensions.SQUARE
        else:
            self.dimensions = dimensions


def delete_generated() -> None:
    """Deletes AI generated images from the filesystem, if present.

    Note: this should only be used when its guaranteed image generation threads
    are not running.
    """
    delete_files = [
        AI_AVATAR_ORIGINAL_PATH,
        AI_AVATAR_PATH,
        AI_HEADER_ORIGINAL_PATH,
        AI_HEADER_PATH,
        AI_STORY_PATH
    ]
    for file in delete_files:
        if os.path.exists(file):
            os.remove(file)
            util.debug(f"Deleted {file}")


def generate_image(prompt: str, dimensions: tuple[int, int]) -> Image.Image | None:
    """Submit an image for generation from Stable Horde.

    `prompt`: the prompt to generate from.
    `dimensions`: the dimensions of the image.
    `seed`: the seed to use for the generation.

    Returns the resulting `Image` or `None` if errors are encountered.
    """
    util.info(f"Generating with prompt: {prompt}")
    steps = int(util.config["horde_steps"])
    for i in range(util.config["max_retries"] * 2):
        try:
            simple_client = AIHordeAPISimpleClient()
            horde_request = simple_client.image_generate_request(
                ImageGenerateAsyncRequest(
                    apikey=util.config["stablehorde_key"],
                    prompt=prompt,
                    models=["Stable Cascade 1.0"],
                    params=ImageGenerationInputPayload(
                        sampler_name="k_euler_a",
                        width=dimensions[0],
                        height=dimensions[1],
                        post_processing=["CodeFormers"],
                        steps=steps,
                        n=2,
                    ),
                    nsfw=False,
                    trusted_workers=True,
                    censor_nsfw=True,
                ),
            )
            uncensored_generation = None
            for gen in horde_request[0].generations:
                if not gen.censored:
                    uncensored_generation = gen
                    break
                # This should work, but the metadata doesn't have it. Need to follow up on the Horde side.
                # is_censored = False
                # for meta in gen.gen_metadata:
                #     if meta.type_ == METADATA_TYPE.censorship:
                #         is_censored = True
                #         util.info(f'Avoiding censored {gen}')
                #         break
                # if not is_censored:
                #     uncensored_generation = gen
                #     break
            if uncensored_generation is None:
                return None
            return simple_client.download_image_from_generation(uncensored_generation)

        except Exception as e:
            util.warn(f"Failed to generate {dimensions[0]}x{dimensions[1]} image with {steps} steps, "\
                f"trying again ({i + 1} / {util.config['max_retries'] * 2}): {e}"
            )
            # Start decreasing steps after max_retries
            if i + 1 >= util.config["max_retries"]:
                steps = max(MIN_STEPS, math.ceil(steps * 0.75))
            sleep(util.config["retry_delay"])

    util.error("Failed to generate AI image.")
    return None


def overlay_image(base: Image.Image, overlay: Image.Image, original_path: str, save_path: str) -> tuple[str, str] | None:
    """Overlays the `overlay` image on top of `base`, saving the result.

    `base`: the base (background/bottom) image.
    `overlay`: the image to overlay on top of `base`.
    `original_path`: the path to save the unmodified `base` to.
    `save_path`: the path to save the modified `base` to.

    Returns the paths of the saved files (original, overlayed), or `None` if errors are encountered.
    """
    tries = 0
    while tries < util.config["max_retries"]:
        tries += 1
        try:
            base.save(original_path, lossless=False, quality=int(util.config["image_quality"]))
            util.debug(f"Saved original AI image to {original_path}")
            base.paste(overlay, (0, 0), overlay)
            base.save(save_path, lossless=False, quality=int(util.config["image_quality"]))
            util.debug(f"Saved new avatar to {save_path}")
            return (original_path, save_path)
        except Exception as e:
            util.warn(f"Error overlaying image: {e}")
            sleep(util.config["retry_delay"])

    return None


def generate_avatar(character, campaign) -> tuple[str, str] | None:
    """Generates a new avatar image based on the given character.

    `character`: the `Character` to generate images from.
    `campaign`: the `Campaign` to generate images from.
    """
    from character import Character
    from campaign import Campaign
    character: Character = character
    campaign: Campaign = campaign
    map = {a: character.ability_score(a) for a in Character.ORDERED_ABILITY_INDEXES}
    max_stat = max(map, key=map.get) # type: ignore[arg-type]

    adjectives = []

    weapon = character.hands[0].name
    armor = character.armor[0].name if character.armor else ""
    adjectives.append(random.choice(ABILITY_TO_ADJ[max_stat]))
    adjectives += RACE_ADJECTIVES.get(character.race.index, [])
    prompt = (
        f"Full body shot of the {', '.join(adjectives)} {character.gender} {character.culture} hero {character.title(True)}. "
        f"they are alone, in {campaign.world.weather()} {campaign.world.location()}. "
        f"They wield a {weapon}, "
        f"{character.clothes.primary} and {character.clothes.secondary} clothes "
        f"{character.complexion} skin"
    )
    if armor:
        prompt += f", they are wearing {armor}."
    prompt += util.config["horde_style"]
    # Negative prompt
    # prompt += "### nude"

    if not os.path.isfile(OVERLAY_AVATAR_PATH):
        util.warn(f"No overlay image provided at {OVERLAY_AVATAR_PATH}")
        util.warn("No avatar AI image will be generated")
        return None

    # Mastodon will scale to 400x400, but our dimensions must be multiples of 16
    generated_image = generate_image(prompt, Dimensions.SQUARE)
    if generated_image is None:
        return None

    overlay_result = overlay_image(generated_image, Image.open(OVERLAY_AVATAR_PATH), AI_AVATAR_ORIGINAL_PATH, AI_AVATAR_PATH)
    if overlay_result is None:
        return None

    return overlay_result


def generate_header(character, campaign) -> tuple[str, str] | None:
    """Generates a new header image based on the given character.

    `character`: the `Character` to generate images from.
    `campaign`: the `Campaign` to generate images from.
    """
    from character import Character
    from campaign import Campaign
    character: Character = character
    campaign: Campaign = campaign
    map = {a: character.ability_score(a) for a in Character.ORDERED_ABILITY_INDEXES}
    max_stat = max(map, key=map.get) # type: ignore[arg-type]

    adjectives = []

    weapon = character.hands[0].name
    armor = character.armor[0].name if character.armor else ""
    adjectives.append(random.choice(ABILITY_TO_ADJ[max_stat]))
    adjectives += RACE_ADJECTIVES.get(character.race.index, [])

    prompt = (
        "Environmental portrait photography of "
        f"{character.gender} {character.culture} {character.title(True)}, alone amidst {campaign.world.weather()} {campaign.world.location()}. "
        f"They are {', '.join(adjectives)}, they are wielding a {weapon}, "
        f"{character.clothes.primary} and {character.clothes.secondary} clothes, "
        f"{character.complexion} skin"
    )
    if armor:
        prompt += f" and are wearing {armor}"
    prompt += util.config["horde_style"]
    # Negative prompt
    prompt += "### duo, many"

    if not os.path.isfile(OVERLAY_HEADER_PATH):
        util.warn(f"No overlay image provided at {OVERLAY_HEADER_PATH}")
        util.warn("No header AI image will be generated")
        return None

    # Mastodon will scale to 1500x500, but our dimensions must be a multiple of 16 with a max of 1024x1024 in total.
    generated_image = generate_image(prompt, Dimensions.ULTRA_WIDE)
    if generated_image is None:
        return None

    overlay_result = overlay_image(generated_image, Image.open(OVERLAY_HEADER_PATH), AI_HEADER_ORIGINAL_PATH, AI_HEADER_PATH)
    if overlay_result is None:
        return None

    return overlay_result


def generate_reply(character, campaign, description: Description) -> str | None:
    """Generates a new story image based on the given character.

    `character`: the `Character` to generate images from.
    `campaign`: the `Campaign` to generate images from.
    `description`: the text description of the current events.
    """
    from character import Character
    from campaign import Campaign
    character: Character = character
    campaign: Campaign = campaign
    weapon_and_armor = ""
    world = ""

    if not description.weather.is_none():
        world += f"in {description.weather()}"
    if not description.location.is_none():
        world += f" {description.location()}"

    if description.hero:
        map = {a: character.ability_score(a) for a in Character.ORDERED_ABILITY_INDEXES}
        max_stat = max(map, key=map.get) # type: ignore[arg-type]

        adjectives = []

        weapon = character.hands[0].nice_name()
        armor = character.armor[0].nice_name() if character.armor else ""
        adjectives.append(random.choice(ABILITY_TO_ADJ[max_stat]))
        adjectives += RACE_ADJECTIVES.get(character.race.index, [])

        if description.weapon:
            weapon_and_armor = f"equipped with a {weapon}"
            if description.armor and armor:
                weapon_and_armor += f", and wearing {armor}."
        elif description.armor and armor:
            weapon_and_armor += f"wearing {armor}."
        pdesc = f"{description.text},"
        if description.style != "":
            pdesc += f" {description.style}"
        prompt = (
            f"{', '.join(adjectives)} {character.gender} {character.culture} hero {character.title(True)} {weapon_and_armor} "
            f"{character.clothes.primary} and {character.clothes.secondary} clothes, "
            f"{character.complexion} skin, "
            f"{pdesc} {world}"
        )
    # If the description is not of a hero, we don't add the hero stuff to the prompt
    else:
        pdesc = description.text
        if description.style != "":
            pdesc += f" {description.style}"
        prompt = (
            f"{pdesc} {world}"
        )

    prompt += util.config["horde_style"]
    # Negative prompt
    # Removed because albedo model doesn't handle negative prompts well
    # prompt += "### nude"

    generated_image = generate_image(
        prompt,
        description.dimensions
    )
    if generated_image is None:
        return None
    generated_image.save(AI_STORY_PATH, lossless=False, quality=int(util.config["image_quality"]))
    return AI_STORY_PATH
