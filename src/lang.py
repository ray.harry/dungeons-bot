# mypy: disable-error-code="attr-defined"

import random
import util
from typing import Any



class LangItem:

    def __init__(self, language: dict, name: str) -> None:
        """Create a new `LangItem`.

        `language`: language dict to use.
        `name`: name (index) of the item.
        """
        self.lang = language[name]
        self.options = []
        if isinstance(self.lang, dict):
            for key in self.lang:
                setattr(self, key, LangItem(self.lang, key))
        elif isinstance(self.lang, list):
            self.options = self.lang
        elif isinstance(self.lang, str):
            self.options = [self.lang]
        else:
            raise TypeError(f"Key {name} is an invalid type.")


    def choose(self, *args: str, option="default") -> str:
        """Choose a string from the `LangItem`'s options.

        `args`: arguments to format the resulting string with.
        `option`: which string category to pick.
        """
        if option in self.__dict__ and isinstance(self[option], LangItem):
            return self[option].choose(*args, option=option)

        if len(self.options) == 0:
            raise IndexError("Invalid language key.")

        return random.choice(self.options).format(*args)


    def __getitem__(self, item) -> "LangItem":
        """Overrides indexing behavior, e.g. LangItem[2]
        """
        return self.__dict__[item]


lang = util.try_load("lang.toml")

# Note: when adding a category to `lang.toml`, an entry must be added here
adventure: Any           = LangItem(lang, "adventure")
attack_character: Any    = LangItem(lang, "attack_character")
attack_monster: Any      = LangItem(lang, "attack_monster")
avoid: Any               = LangItem(lang, "avoid")
bio_campaign: Any        = LangItem(lang, "bio_campaign")
bio_character: Any       = LangItem(lang, "bio_character")
bio_monster: Any         = LangItem(lang, "bio_monster")
chest: Any               = LangItem(lang, "chest")
command_reply: Any       = LangItem(lang, "command_reply")
create: Any              = LangItem(lang, "create")
death_character: Any     = LangItem(lang, "death_character")
death_monster: Any       = LangItem(lang, "death_monster")
effects_character: Any   = LangItem(lang, "effects_character")
fight: Any               = LangItem(lang, "fight")
options: Any             = LangItem(lang, "options")
purchase: Any            = LangItem(lang, "purchase")
rest_long: Any           = LangItem(lang, "rest_long")
rest_short: Any          = LangItem(lang, "rest_short")
retire: Any              = LangItem(lang, "retire")
reward: Any              = LangItem(lang, "reward")
run_character: Any       = LangItem(lang, "run_character")
sell: Any                = LangItem(lang, "sell")
shop: Any                = LangItem(lang, "shop")
stranger: Any            = LangItem(lang, "stranger")
town: Any                = LangItem(lang, "town")
trader: Any              = LangItem(lang, "trader")
treasure: Any            = LangItem(lang, "treasure")
