import dnd.dndclient as dndclient


class Subclass:
    """D&D Subclass
    """
    def __init__(self, subclass_dict: dndclient.DNDResponse) -> None:

        self._dnd_data = subclass_dict.reduce()
        self.index = str(subclass_dict["index"])
        self.name = str(subclass_dict["name"])
        self.desc = "\n".join(subclass_dict["desc"])
        self.clazz = dict(subclass_dict["class"])
        self.subclass_flavor = str(subclass_dict["subclass_flavor"])
        self.subclass_levels = str(subclass_dict["subclass_levels"])
        self.spells: list[dict] = list(subclass_dict["spells"])


    def __hash__(self) -> int:

        return hash(self.index)


    def __eq__(self, __value: object) -> bool:

        if not isinstance(self, __value.__class__):
            return False

        return hash(self) == hash(__value)
