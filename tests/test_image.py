from constants import Dimensions
import image


def test_dimensions() -> None:

    for name, value in Dimensions.__dict__.items():
        if name.startswith("__"): continue
        assert value[0] % 64.0 == 0
        assert value[1] % 64.0 == 0
        assert value[0] * value[1] <= 1024 * 1024
