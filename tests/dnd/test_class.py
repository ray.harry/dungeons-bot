from dnd.clazz import Class
import dnd.dndclient as dndclient


def test_init() -> None:

    prev_class = None
    for c in dndclient.get_classes():
        dnd_class = dndclient.get_class(c["index"])
        clazz = Class(dnd_class)
        assert clazz
        assert clazz.index == dnd_class["index"]
        assert clazz == clazz
        if prev_class is not None:
            assert prev_class != clazz
        prev_class = clazz
