import pytest

import dnd.dndclient as dndclient
from dnd.monster import Monster


N = 10


@pytest.fixture
def monster() -> Monster:

    monster_dict = dndclient.get_monster("aboleth")
    return Monster(monster_dict)


def test_init(monster: Monster) -> None:

    assert monster.hitpoints == monster.max_hitpoints

    assert len(monster.saving_throws) == len(monster.abilities)
    assert len(monster.skills) > 0

    assert monster.proficiency_bonus == dndclient.cr_proficiency(monster.challenge_rating)


def test_armor_class(monster: Monster) -> None:

    assert monster.armor_class() >= 0


def test_emoji(monster: Monster) -> None:

    assert isinstance(monster.emoji(), str)


def test_relative_difficulty(monster: Monster) -> None:

    for l in range(1, 21):
        assert monster.relative_difficulty(l) in [-2, -1, 0, 1, 2]


def test_challenge_rating_emoji(monster: Monster) -> None:

    assert len(monster.challenge_rating_emoji()) > 0
    for l in range(1, 21):
        assert len(monster.challenge_rating_emoji(l)) > 0


def test_short_bio(monster: Monster) -> None:

    assert len(monster.short_bio()) > 0


def test_bio(monster: Monster) -> None:

    assert len(monster.bio()) > 0


def test_bio_extended(monster: Monster) -> None:

    assert len(monster.extended_bio()) > 0


def test_drops(monster: Monster) -> None:

    drops = monster.drops()
    assert isinstance(drops, set)


def test_modify_damage(monster: Monster) -> None:

    # resistant
    quasit = Monster(dndclient.get_monster("quasit"))
    assert monster.modify_damage(quasit, 10, "cold") == 5
    assert monster.modify_damage(quasit, 10, "fire") == 5
    assert monster.modify_damage(quasit, 10, "lightning") == 5
    assert monster.modify_damage(quasit, 10, "acid") == 10

    # immune
    mephit = Monster(dndclient.get_monster("magma-mephit"))
    assert monster.modify_damage(mephit, 10, "fire") == 1
    assert monster.modify_damage(mephit, 10, "poison") == 1
    assert monster.modify_damage(mephit, 10, "acid") == 10

    # vulnerable
    skeleton = Monster(dndclient.get_monster("skeleton"))
    assert monster.modify_damage(skeleton, 10, "bludgeoning") == 20
    assert monster.modify_damage(skeleton, 10, "cold") == 10


def test_get_attacks(monster: Monster) -> None:

    assert isinstance(monster.get_attacks(), list)


def test_get_attacks2() -> None:

    bandit = Monster(dndclient.get_monster("bandit"))
    assert len(bandit.get_attacks()) == 2

    archmage = Monster(dndclient.get_monster("archmage"))
    assert len(archmage.get_attacks()) == 7

    aboleth = Monster(dndclient.get_monster("aboleth"))
    assert len(aboleth.get_attacks()) == 3

    ancient_black_dragon = Monster(dndclient.get_monster("ancient-black-dragon"))
    assert len(ancient_black_dragon.get_attacks()) == 5


def test_attack(monster: Monster) -> None:

    for attack in monster.get_attacks():
        result = monster.attack(monster, attack)

        if result.attack_hit.value <= 0:
            assert result.damage == 0
        else:
            assert result.damage > 0


def test_all_monsters() -> None:

    monsters = dndclient.get_monsters()

    for m in monsters:
        monster = Monster(dndclient.get_monster(m["index"]))
        test_init(monster)
        test_armor_class(monster)
        test_emoji(monster)
        test_short_bio(monster)
        test_bio(monster)
        test_attack(monster)
