import pytest

from dnd.equipment import Equipment
import dnd.dndclient as dndclient
from currency import Currency, Unit

def test_init() -> None:

    club = dndclient.get_equipment("club")
    equipment = Equipment(club)
    assert equipment.name == "Club"
    assert equipment.equipment_category == "weapon"
    assert equipment.cost == Currency(1, Unit.gp)
    assert len(equipment.properties) == 3

    equipment = Equipment(dndclient.get_equipment("leather-armor"))
    equipment = Equipment(dndclient.get_equipment("shield"))


def test_is_armor() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert not club.is_armor()

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    assert leather_armor.is_armor()

    shield = Equipment(dndclient.get_equipment("shield"))
    assert shield.is_armor()


def test_is_weapon() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert club.is_weapon()

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    assert not leather_armor.is_weapon()

    shield = Equipment(dndclient.get_equipment("shield"))
    assert not shield.is_weapon()

    net = Equipment(dndclient.get_equipment("net"))
    assert not net.is_weapon()


def test_get_emoji() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert isinstance(club.get_emoji(), str)
    assert len(club.get_emoji()) > 0

    greatsword = Equipment(dndclient.get_equipment("greatsword"))
    assert isinstance(greatsword.get_emoji(), str)
    assert len(greatsword.get_emoji()) > 0

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    assert isinstance(leather_armor.get_emoji(), str)
    assert len(leather_armor.get_emoji()) > 0


def test_get_property_emojis() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert len(club.get_property_emojis()) > 0


def test_has_property() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert club.has_property("light")
    assert not club.has_property("thrown")


def test_set_silvered() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    initial_cost = club.cost
    assert not club.silvered
    club.set_silvered(True)
    assert club.silvered
    assert club.cost == initial_cost + Currency(50)


def test_is_shield() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert not club.is_shield()

    shield = Equipment(dndclient.get_equipment("shield"))
    assert shield.is_shield()

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    assert not leather_armor.is_shield()


def test_is_aquatic_weapon() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert not club.is_aquatic_weapon()

    trident = Equipment(dndclient.get_equipment("trident"))
    assert trident.is_aquatic_weapon()

    leather_armor = Equipment(dndclient.get_equipment("leather-armor"))
    assert not leather_armor.is_aquatic_weapon()


def test_get_weapon_ability() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert club.get_weapon_ability() == ["str"]

    rapier = Equipment(dndclient.get_equipment("rapier"))
    assert rapier.get_weapon_ability() == ["str", "dex"]


def test_get_value() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert club.cost == Currency(1, Unit.gp)

    greatsword = Equipment(dndclient.get_equipment("greatsword"))
    assert greatsword.cost == Currency(50, Unit.gp)


def test_nice_name() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    assert club.nice_name() == "Club"

    light_crossbow = Equipment(dndclient.get_equipment("crossbow-light"))
    assert light_crossbow.nice_name() == "Light Crossbow"

    hand_crossbow = Equipment(dndclient.get_equipment("crossbow-hand"))
    assert hand_crossbow.nice_name() == "Hand Crossbow"


def test_net_spend() -> None:

    club = Equipment(dndclient.get_equipment("club"))
    longsword = Equipment(dndclient.get_equipment("longsword"))
    assert Equipment.net_spend([club], [longsword]) == Currency(14.5, Unit.gp)
    assert Equipment.net_spend([longsword], [club]) == Currency(-6.5, Unit.gp)
