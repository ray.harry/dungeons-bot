from world import World, Location, Weather


def test_world() -> None:

    world = World()
    assert len(world.location()) > 0
    assert len(world.weather()) > 0
    world.reroll_location()
    assert len(world.location()) > 0
    world.reroll_weather()
    assert len(world.weather()) > 0


def test_location() -> None:

    location = Location()
    assert len(location()) > 0

    location = Location("test")
    assert location() == "test"

    location = Location.none()
    assert location() == "none"
    assert location.is_none()


def test_weather() -> None:

    weather = Weather()
    assert len(weather()) > 0

    weather = Weather("test")
    assert weather() == "test"

    weather = Weather.none()
    assert weather() == "none"
    assert weather.is_none()
