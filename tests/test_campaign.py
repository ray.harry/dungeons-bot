import pytest
from campaign import Campaign


@pytest.fixture()
def campaign() -> Campaign:

    return Campaign()


def test_init() -> None:

    campaign = Campaign()
    assert campaign
