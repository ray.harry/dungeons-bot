from dice import Dice, _DiceElement

N = 10


def test_dice_element() -> None:

    dice_element = _DiceElement("4")
    assert dice_element.sign == 1
    assert dice_element.die_count == 4
    assert dice_element.face_count == 1

    dice_element = _DiceElement("+4")
    assert dice_element.sign == 1
    assert dice_element.die_count == 4
    assert dice_element.face_count == 1

    dice_element = _DiceElement("-4")
    assert dice_element.sign == -1
    assert dice_element.die_count == 4
    assert dice_element.face_count == 1

    dice_element = _DiceElement("3d4")
    assert dice_element.sign == 1
    assert dice_element.die_count == 3
    assert dice_element.face_count == 4

    dice_element = _DiceElement("+3d4")
    assert dice_element.sign == 1
    assert dice_element.die_count == 3
    assert dice_element.face_count == 4

    dice_element = _DiceElement("-3d4")
    assert dice_element.sign == -1
    assert dice_element.die_count == 3
    assert dice_element.face_count == 4


def test_init() -> None:

    dice = Dice("3d4+3-1")
    assert dice
    assert dice.dice == "3d4+3-1"
    assert str(dice) == "3d4+3-1"
    assert len(dice.elements) == 3

    dice = Dice("3d4+3-1+MOD")
    assert dice
    assert dice.dice == "3d4+3-1+MOD"
    assert str(dice) == "3d4+3-1+MOD"
    assert len(dice.elements) == 4

    dice = Dice("3d4+3-1+MOD-MOD")
    assert dice
    assert dice.dice == "3d4+3-1+MOD-MOD"
    assert str(dice) == "3d4+3-1+MOD-MOD"
    assert len(dice.elements) == 5

    dice = Dice("3d4+3-1+9d6-1")
    assert dice
    assert dice.dice == "3d4+3-1+9d6-1"
    assert str(dice) == "3d4+3-1+9d6-1"
    assert len(dice.elements) == 5

    dice = Dice("4")
    assert dice
    assert dice.dice == "4"
    assert str(dice) == "4"
    assert len(dice.elements) == 1

    dice = Dice(3, 4)
    assert dice
    assert dice.dice == "3d4"


def test_is_dice() -> None:

    assert Dice.is_dice("4")
    assert Dice.is_dice("3d4")
    assert Dice.is_dice("3d4+3")
    assert Dice.is_dice("3d4+3-1")
    assert Dice.is_dice("3d4+3-4")
    assert Dice.is_dice("3d4+3-4+MOD")
    assert Dice.is_dice("3d4 + 3 - 4 + MOD")
    assert Dice.is_dice("3d4 + 9d6 + 3 + MOD + 3d4")

    assert not Dice.is_dice("")
    assert not Dice.is_dice("3d")
    assert not Dice.is_dice("d4")
    assert not Dice.is_dice("3d4d")
    assert not Dice.is_dice("d3d4")
    assert not Dice.is_dice("3d4+3-1d")


def test_roll() -> None:

    dice = Dice("3d4+3-1")
    for _ in range(N):
        assert 5 <= dice.roll() <= 14
        assert 10 <= dice.roll(2) <= 28
        assert 5 <= dice.roll(2, highest=1) <= 14
        assert 5 <= dice.roll(2, lowest=1) <= 14
        assert 10 <= dice.roll(10, lowest=1, highest=1) <= 28
        assert 10 <= dice.roll(5, highest=2) <= 28
        assert 10 <= dice.roll(5, lowest=2) <= 28
        assert 25 <= dice.roll(10, lowest=2, highest=3) <= 70
        assert 50 <= dice.roll(10, lowest=8, highest=3) <= 140

    dice = Dice("3d4+6+3d4-4")
    for _ in range(N):
        assert 8 <= dice.roll() <= 26

    dice = Dice("3d4+6+3d4-4+MOD")
    for _ in range(N):
        assert 18 <= dice.roll(custom_mod=10) <= 36

    dice = Dice("3d4+6-MOD+3d4-4+MOD")
    for _ in range(N):
        assert 8 <= dice.roll(custom_mod=10) <= 26

    dice = Dice("4")
    for _ in range(N):
        assert dice.roll() == 4
        assert dice.roll(2) == 8
        assert dice.roll(10, highest=1) == 4
        assert dice.roll(10, lowest=1) == 4
        assert dice.roll(10, highest=1, lowest=1) == 8
        assert dice.roll(10, highest=8, lowest=3) == 40


def test_max() -> None:

    dice = Dice("3d4+3-1")
    assert dice.max() == 14

    dice = Dice("4")
    assert dice.max() == 4


def test_min() -> None:

    dice = Dice("3d4+3-1")
    assert dice.min() == 5

    dice = Dice("4")
    assert dice.min() == 4


def test_mean() -> None:

    dice = Dice("3d4+3-1")
    assert dice.mean() == 9.5

    dice = Dice("4")
    assert dice.mean() == 4
